# Odoo Purchase Products Only From Vendors

Customization to only show the vendor's products in the purchase_order_form.
* INHERIT purchase.order.form.inherit (form)

### Realease process

To release and publish the package to PyPi:

1. Update the version in `__manifest__.py`.
2. Open a merge request with these changes for the team to approve.
3. Merge it, add a git tag on that merge commit and push it.
4. Once the pipeline has successfully passed, go approve the publish step.